package com.avocado.amcmakeathon.model.data

import java.io.Serializable

data class Place(
    val name: String = "",
    val category: String = "",
    val urlPhoto: String = "",
    val description: String = "",
    val lat: Double = 0.0,
    val lng: Double = 0.0,
    val currentFullness: Int = 0,
    val maxFullness: Int = 0,
    val workingHours: String = "",
    val queueId: String = ""
) : Serializable