package com.avocado.amcmakeathon.model.interactor

import com.avocado.amcmakeathon.model.data.Place
import com.avocado.amcmakeathon.model.repository.PlaceRepository

class PlaceInteractor(
    private val placeRepository: PlaceRepository
) {

    suspend fun getPlaces(): List<Place> {
        return placeRepository.getPlaces()
            .sortedBy { it.currentFullness.toDouble() / it.maxFullness }
    }
}