package com.avocado.amcmakeathon.model.repository

import com.avocado.amcmakeathon.api.PlacesApi

class PlaceRepository(
    private val placesApi: PlacesApi
) {

    suspend fun getPlaces() = placesApi.getPlaces().response
}