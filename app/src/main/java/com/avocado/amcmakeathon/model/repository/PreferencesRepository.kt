/*
 * Copyright 2020 Andrey Kitaev. All rights reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * http://www.apache.org/licenses/LICENSE-2.0
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.avocado.amcmakeathon.model.repository

import android.content.Context
import android.content.SharedPreferences
import com.avocado.amcmakeathon.ProjectApplication
import com.avocado.amcmakeathon.utils.KeyStore

class PreferencesRepository {

    private val preferences: SharedPreferences by lazy {
        ProjectApplication.appInstance.applicationContext.getSharedPreferences(
            KeyStore.SHARED_PREFERENCES_KEY,
            Context.MODE_PRIVATE
        )
    }

    fun isFirstLaunch(): Boolean {
        return false // TODO(CHANGE IT)
        //return preferences.getBoolean(KeyStore.FIRST_LAUNCH_KEY, true)
    }

    fun setFirstLaunch(isFirstLaunch: Boolean) {
        preferences.edit()
            .putBoolean(KeyStore.FIRST_LAUNCH_KEY, isFirstLaunch)
            .apply()
    }

    fun clearPreferences() {
        preferences.edit()
            .clear()
            .apply()
    }
}