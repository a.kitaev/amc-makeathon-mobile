package com.avocado.amcmakeathon.ui.holdStart

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.avocado.amcmakeathon.utils.observeNullable
import com.qwaey.eventpicker.R
import org.koin.androidx.viewmodel.ext.android.viewModel

class HoldStartFragment : Fragment() {

    private val viewModel by viewModel<HoldStartViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.handleHoldStart()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        initObservers()
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun initObservers() = with(viewModel) {
        navigationLive.observeNullable(this@HoldStartFragment) {
            when (it) {
                HoldStartNavigation.MAIN -> findNavController().navigate(R.id.action_hold_start_fragment_to_main_fragment)
                HoldStartNavigation.WIZARD -> findNavController().navigate(R.id.action_hold_start_fragment_to_wizard_fragment)
                else -> {
                    /* wait other state */
                }
            }
        }
    }
}