package com.avocado.amcmakeathon.ui.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.avocado.amcmakeathon.model.data.Place
import com.avocado.amcmakeathon.utils.toMarker
import kotlinx.coroutines.launch

class MapViewModel : ViewModel() {

    val placeMarkerLiveData = MutableLiveData<List<PlaceMarker>>()
    val bottomSheetData = MutableLiveData<PlaceMarker>()

    fun handlePlaceUpdate(places: List<Place>) {
        viewModelScope.launch {
            convertPlaces(places)
        }
    }

    fun handleMarkerClickListener(place: PlaceMarker?) {
        bottomSheetData.postValue(place)
    }

    private fun convertPlaces(places: List<Place>) {
        placeMarkerLiveData.postValue(
            places.map { it.toMarker() }
        )
    }
}