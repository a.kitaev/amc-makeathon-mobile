package com.avocado.amcmakeathon.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.avocado.amcmakeathon.ui.details.DetailsFragment
import com.avocado.amcmakeathon.ui.list.ListFragment
import com.avocado.amcmakeathon.ui.map.MapFragment
import com.avocado.amcmakeathon.utils.observeNullable
import com.avocado.amcmakeathon.utils.showMessage
import com.qwaey.eventpicker.R
import com.qwaey.eventpicker.databinding.FragmentMainBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment() {

    private val viewModel by viewModel<MainViewModel>()
    private lateinit var binding: FragmentMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadEvents()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(layoutInflater)
        initUI()
        initObservers()
        return binding.root
    }

    private fun initUI() {
        val adapter = ViewPagerAdapter(childFragmentManager)
        adapter.addFragment(
            MapFragment(
                viewModel.placesLiveData,
                viewModel.navigationPlaceLiveData
            ), "Карта"
        )
        adapter.addFragment(
            ListFragment(
                viewModel.placesLiveData,
                viewModel.navigationPlaceLiveData
            ), "Список"
        )
        binding.viewPager.adapter = adapter
        binding.tabLayout.setupWithViewPager(binding.viewPager)
    }

    private fun initObservers() = with(viewModel) {
        errorMessageLiveData.observeNullable(this@MainFragment) {
            showMessage(it)
        }

        navigationPlaceLiveData.observeNullable(this@MainFragment) {
            viewModel.navigationPlaceLiveData.value = null
            val bundle = Bundle().apply {
                putSerializable(DetailsFragment.PLACE_KEY, it)
            }
            findNavController().navigate(R.id.toDetails, bundle)
        }
    }
}