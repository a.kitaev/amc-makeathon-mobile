package com.avocado.amcmakeathon.ui.holdStart

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.avocado.amcmakeathon.model.repository.PreferencesRepository

class HoldStartViewModel(
    private val preferencesRepository: PreferencesRepository
) : ViewModel() {

    val navigationLive = MutableLiveData(HoldStartNavigation.INITIAL)

    fun handleHoldStart() {
        if (preferencesRepository.isFirstLaunch()) {
            navigationLive.postValue(HoldStartNavigation.WIZARD)
        } else {
            navigationLive.postValue(HoldStartNavigation.MAIN)
        }
    }
}