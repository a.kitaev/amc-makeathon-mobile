package com.avocado.amcmakeathon.ui.webview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.avocado.amcmakeathon.utils.AppWebClient
import com.qwaey.eventpicker.R
import kotlinx.android.synthetic.main.fragment_webview.*

class WebViewFragment : Fragment() {

    private val args: WebViewFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_webview, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        webView.apply {
            settings.javaScriptEnabled = true
            settings.domStorageEnabled = true
            webChromeClient = WebChromeClient()
            webViewClient = AppWebClient()
            addJavascriptInterface(JsInterface(), "AndroidFunction")
        }
        webView.loadUrl("https://pushkov-fedor.github.io/amc-frontend")
    }

    private inner class JsInterface {

        @JavascriptInterface
        fun getId(): String {
            return args.queueId
        }

        @JavascriptInterface
        fun back() {
            findNavController().popBackStack()
        }
    }
}