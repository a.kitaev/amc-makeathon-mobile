package com.avocado.amcmakeathon.ui.details

import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.avocado.amcmakeathon.model.data.Place
import com.avocado.amcmakeathon.model.providers.ImageProvider
import com.qwaey.eventpicker.databinding.FragmentDetailsBinding
import java.util.*

class DetailsFragment : Fragment() {

    private val geocoder by lazy { Geocoder(requireContext(), Locale.getDefault()) }
    private val imageProvider = ImageProvider()
    private lateinit var binding: FragmentDetailsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(layoutInflater)
        initUI()
        return binding.root
    }

    private fun initUI() {
        arguments?.getSerializable(PLACE_KEY).let {
            if (it is Place) {
                imageProvider.loadImage(it.urlPhoto, binding.image)
                binding.title.text = it.name
                binding.type.text = it.category
                binding.time.text = it.workingHours
                val res = geocoder.getFromLocation(it.lat, it.lng, 1)[0]
                binding.place.text = res.getAddressLine(0)
                binding.fullness.text = getZoneWithPercent(it)

                binding.wantToVisit.setOnClickListener { _ ->
                    findNavController().navigate(DetailsFragmentDirections.toWebView(it.queueId))
                }
            }
        }

        binding.back.setOnClickListener {
            findNavController().navigateUp()
        }
    }

    private fun getZoneWithPercent(place: Place): String {
        val percent = 100 * place.currentFullness / place.maxFullness
        return when (percent) {
            in 0..30 -> "Благожелательная зона ($percent%)"
            in 31..60 -> "Зона среднего риска ($percent%)"
            in 61..90 -> "Зона риска ($percent%)"
            else -> "Зона высокого риска ($percent%)"
        }
    }

    companion object {
        const val PLACE_KEY = "PLACE_KEY"
    }
}