package com.avocado.amcmakeathon.ui.map

import android.location.Geocoder
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import com.avocado.amcmakeathon.model.data.Place
import com.avocado.amcmakeathon.utils.getColorRes
import com.avocado.amcmakeathon.utils.observeNullable
import com.avocado.amcmakeathon.utils.toPlace
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.qwaey.eventpicker.R
import com.qwaey.eventpicker.databinding.ComponentBottomSheetBinding
import com.qwaey.eventpicker.databinding.FragmentMapBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class MapFragment(
    private val places: MutableLiveData<List<Place>>,
    private val navigationPlace: MutableLiveData<Place>
) : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private val viewModel by viewModel<MapViewModel>()
    private val geocoder by lazy {
        Geocoder(requireContext(), Locale.getDefault())
    }

    private lateinit var map: Map
    private lateinit var binding: FragmentMapBinding
    private lateinit var bottomSheet: BottomSheetBehavior<View>
    private lateinit var bottomSheetBinding: ComponentBottomSheetBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapBinding.inflate(layoutInflater)
        bottomSheetBinding = ComponentBottomSheetBinding.inflate(
            layoutInflater,
            binding.bottomSheetContainer,
            true
        )
        initUI()
        initObservers()
        return binding.root
    }

    private fun initUI() {
        map = MapController(this)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        places.observeNullable(this) {
            viewModel.handlePlaceUpdate(it)
        }
        bottomSheet = BottomSheetBehavior.from(bottomSheetBinding.root)
    }

    private fun initObservers() = with(viewModel) {
        placeMarkerLiveData.observeNullable(this@MapFragment) {
            map.updateVisibleMarkers(it)
        }

        bottomSheetData.observeNullable(this@MapFragment) {
            setPlace(it)
            bottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    private fun setPlace(place: PlaceMarker) {
        bottomSheetBinding.item.title.text = place.name
        bottomSheetBinding.item.type.text = place.category
        bottomSheetBinding.item.time.text = place.workingHours
        val res = geocoder.getFromLocation(place.lat, place.lng, 1)[0]
        bottomSheetBinding.item.place.text = res.getAddressLine(0)
        bottomSheetBinding.item.card.setCardBackgroundColor(
            ContextCompat.getColor(
                requireContext(),
                getColorRes(place)
            )
        )
        bottomSheetBinding.item.card.setOnClickListener {
            navigationPlace.postValue(place.toPlace())
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map.onAttachMainMap(requireContext(), googleMap)
        map.moveTo(
            LatLng(55.761088, 37.631204), // Moscow
            false,
            14f
        )

        // as a precaution
        viewModel.placeMarkerLiveData.value?.let {
            map.updateVisibleMarkers(it)
        }
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        map.moveTo(marker.position, true)
        val placeMarker = map.getPlaceById(marker.id)
        viewModel.handleMarkerClickListener(placeMarker)
        return true
    }
}
