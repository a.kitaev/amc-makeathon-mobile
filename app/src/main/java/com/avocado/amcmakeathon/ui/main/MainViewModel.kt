package com.avocado.amcmakeathon.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.avocado.amcmakeathon.model.data.Place
import com.avocado.amcmakeathon.model.interactor.PlaceInteractor
import com.avocado.amcmakeathon.model.providers.ResourceProvider
import com.qwaey.eventpicker.R
import kotlinx.coroutines.launch
import timber.log.Timber

class MainViewModel(
    private val placeInteractor: PlaceInteractor,
    private val resourceProvider: ResourceProvider
) : ViewModel() {

    val navigationPlaceLiveData = MutableLiveData<Place>()
    val placesLiveData = MutableLiveData(listOf<Place>())
    val errorMessageLiveData = MutableLiveData<String>()

    fun loadEvents() {
        viewModelScope.launch {
            loadEventsFromBackend()
        }
    }

    private suspend fun loadEventsFromBackend() {
        try {
            val places = placeInteractor.getPlaces()
            placesLiveData.postValue(places)
        } catch (e: Exception) {
            Timber.e(e, "loadEventsFromBackend: failure")
            errorMessageLiveData.postValue(resourceProvider.getString(R.string.error_network))
        }
    }
}
