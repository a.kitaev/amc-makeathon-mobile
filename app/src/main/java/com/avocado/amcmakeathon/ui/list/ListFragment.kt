package com.avocado.amcmakeathon.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.LinearLayoutManager
import com.avocado.amcmakeathon.model.data.Place
import com.avocado.amcmakeathon.utils.observeNullable
import com.qwaey.eventpicker.databinding.FragmentListBinding
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListFragment(
    private val places: MutableLiveData<List<Place>>,
    private val navigationPlace: MutableLiveData<Place>
) : Fragment() {

    private val viewModel by viewModel<ListViewModel>()
    private lateinit var binding: FragmentListBinding
    private lateinit var placesAdapter: PlaceListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(layoutInflater)
        initUI()
        return binding.root
    }

    private fun initUI() {
        val layoutManager = LinearLayoutManager(context)
        binding.recycler.layoutManager = layoutManager
        placesAdapter = PlaceListAdapter(requireContext()) {
            navigationPlace.postValue(it)
        }
        binding.recycler.adapter = placesAdapter

        places.observeNullable(this) {
            placesAdapter.setPlaces(it)
        }
    }
}