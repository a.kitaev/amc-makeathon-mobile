package com.avocado.amcmakeathon.ui.map

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng

interface Map {

    fun addMarker(marker: PlaceMarker)
    fun clearMarkers()
    fun updateVisibleMarkers(markers: Collection<PlaceMarker>)
    fun moveTo(latLng: LatLng, isAnimate: Boolean, zoom: Float? = null)
    fun onAttachMainMap(context: Context, googleMap: GoogleMap)
    fun getPlaceById(id: String): PlaceMarker?
}