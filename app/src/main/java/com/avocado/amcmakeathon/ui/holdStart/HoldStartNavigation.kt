package com.avocado.amcmakeathon.ui.holdStart

enum class HoldStartNavigation {
    INITIAL,
    WIZARD,
    MAIN
}