package com.avocado.amcmakeathon.ui.list

import android.content.Context
import android.location.Geocoder
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.avocado.amcmakeathon.model.data.Place
import com.qwaey.eventpicker.R
import com.qwaey.eventpicker.databinding.ComponentPlacesListItemBinding
import com.qwaey.eventpicker.databinding.ComponentWarningSeparatorBinding
import java.util.*
import kotlin.collections.ArrayList

class PlaceListAdapter(
    private val context: Context,
    private val callback: (Place) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val geocoder = Geocoder(context, Locale.getDefault())
    private val items = ArrayList<Item>()

    fun setPlaces(places: List<Place>) {
        synchronized(items) {
            items.clear()
            addPlacesPart(
                "Благожелательная зона",
                places.filter { getPercent(it) < 30 },
                R.color.color_green
            )
            addPlacesPart(
                "Зона среднего риска",
                places.filter { getPercent(it) in 31..60 },
                R.color.color_yellow
            )
            addPlacesPart(
                "Зона риска",
                places.filter { getPercent(it) in 61..90 },
                R.color.color_orange
            )
            addPlacesPart(
                "Зона высокого риска",
                places.filter { getPercent(it) > 90 },
                R.color.color_red
            )
            notifyDataSetChanged()
        }
    }

    private fun getPercent(place: Place): Int {
        return 100 * place.currentFullness / place.maxFullness
    }

    private fun addPlacesPart(title: String, places: List<Place>, @ColorRes color: Int) {
        if (places.isNotEmpty()) {
            items.add(TitleItem("$title (${places.count()})"))
            items.addAll(places.map { PlaceItem(it, color) })
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            TYPE_EVENT -> PlaceHolder(
                ComponentPlacesListItemBinding.inflate(inflater, parent, false)
            )
            TYPE_TITLE -> TitleHolder(
                ComponentWarningSeparatorBinding.inflate(inflater, parent, false)
            )
            else -> throw IllegalStateException("viewType not implemented")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when (item.viewType) {
            TYPE_EVENT -> (holder as PlaceHolder).setPlace(
                (item as PlaceItem).place,
                item.color
            )
            TYPE_TITLE -> (holder as TitleHolder).setTitle((item as TitleItem).text)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].viewType
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class PlaceHolder(
        private val binding: ComponentPlacesListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun setPlace(place: Place, @ColorRes color: Int) {
            binding.title.text = place.name
            binding.type.text = place.category
            binding.time.text = place.workingHours
            val res = geocoder.getFromLocation(place.lat, place.lng, 1)[0]
            binding.place.text = res.getAddressLine(0)
            binding.card.setCardBackgroundColor(ContextCompat.getColor(context, color))
            binding.card.setOnClickListener {
                callback.invoke(place)
            }
        }
    }

    inner class TitleHolder(
        private val binding: ComponentWarningSeparatorBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun setTitle(text: String) {
            binding.text.text = text
        }
    }

    interface Item {
        val viewType: Int
    }

    class PlaceItem(val place: Place, @ColorRes val color: Int) : Item {
        override val viewType: Int
            get() = TYPE_EVENT
    }

    class TitleItem(val text: String) : Item {
        override val viewType: Int
            get() = TYPE_TITLE
    }

    companion object {
        private const val TYPE_EVENT = 1
        private const val TYPE_TITLE = 2
    }
}