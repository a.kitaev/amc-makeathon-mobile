package com.avocado.amcmakeathon.ui.base

interface BackPressedCallbackResolver {

    fun addBackCallback(key: Int, callback: () -> Unit)
    fun removeBackCallback(key: Int)
    fun callParentCallback(key: Int)
}