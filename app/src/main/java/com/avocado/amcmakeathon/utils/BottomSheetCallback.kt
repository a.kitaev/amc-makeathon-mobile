package com.avocado.amcmakeathon.utils

import android.view.View
import com.google.android.material.bottomsheet.BottomSheetBehavior

class BottomSheetCallback(
    private val onSlide: ((View, Float) -> Unit)? = null,
    private val onStateChanged: ((View, Int) -> Unit)? = null,
) : BottomSheetBehavior.BottomSheetCallback() {

    override fun onStateChanged(bottomSheet: View, newState: Int) {
        onStateChanged?.invoke(bottomSheet, newState)
    }

    override fun onSlide(bottomSheet: View, slideOffset: Float) {
        onSlide?.invoke(bottomSheet, slideOffset)
    }
}