package com.avocado.amcmakeathon.utils

import com.avocado.amcmakeathon.model.data.Place
import com.avocado.amcmakeathon.ui.map.PlaceMarker
import com.google.android.gms.maps.model.LatLng
import com.qwaey.eventpicker.R
import java.util.*
import kotlin.random.Random

fun Place.toMarker(): PlaceMarker {
    val percent = 100 * this.currentFullness / maxFullness
    val image = when {

        // sorry about this
        category.isFood() and percent.isGreen() -> R.drawable.ic_food_green_marker
        category.isFood() and percent.isYellow() -> R.drawable.ic_food_yellow_marker
        category.isFood() and percent.isOrange() -> R.drawable.ic_food_orange_marker
        category.isFood() and percent.isRed() -> R.drawable.ic_food_red_marker

        category.isShop() and percent.isGreen() -> R.drawable.ic_shop_green_marker
        category.isShop() and percent.isYellow() -> R.drawable.ic_shop_yellow_marker
        category.isShop() and percent.isOrange() -> R.drawable.ic_shop_orange_marker
        category.isShop() and percent.isRed() -> R.drawable.ic_shop_red_marker

        category.isSport() and percent.isGreen() -> R.drawable.ic_sport_green_marker
        category.isSport() and percent.isYellow() -> R.drawable.ic_sport_yellow_marker
        category.isSport() and percent.isOrange() -> R.drawable.ic_sport_orange_marker
        category.isSport() and percent.isRed() -> R.drawable.ic_sport_red_marker

        category.isBarbershop() and percent.isGreen() -> R.drawable.ic_barbershop_green_marker
        category.isBarbershop() and percent.isYellow() -> R.drawable.ic_barbershop_yellow_marker
        category.isBarbershop() and percent.isOrange() -> R.drawable.ic_barbershop_orange_marker
        category.isBarbershop() and percent.isRed() -> R.drawable.ic_barbershop_red_marker

        else -> R.drawable.ic_pet_green_marker
    }
    return PlaceMarker(
        id = Random.nextLong(),
        latLng = LatLng(this.lat, this.lng),
        avatarImageRes = image,
        name = this.name,
        category = this.category,
        urlPhoto = this.urlPhoto,
        description = this.description,
        lat = this.lat,
        lng = this.lng,
        currentFullness = this.currentFullness,
        maxFullness = this.maxFullness,
        workingHours = this.workingHours,
        queueId = this.queueId
    )
}

fun PlaceMarker.toPlace() = Place(
    name = this.name,
    category = this.category,
    urlPhoto = this.urlPhoto,
    description = this.description,
    lat = this.lat,
    lng = this.lng,
    currentFullness = this.currentFullness,
    maxFullness = this.maxFullness,
    workingHours = this.workingHours,
    queueId = this.queueId
)

private fun Int.isGreen() = this < 30
private fun Int.isYellow() = this in 31..60
private fun Int.isOrange() = this in 61..90
private fun Int.isRed() = this > 90

private fun String.isFood() = this.toLowerCase(Locale.getDefault()) == "еда"
private fun String.isShop() = this.toLowerCase(Locale.getDefault()) == "магазин"
private fun String.isSport() = this.toLowerCase(Locale.getDefault()) == "спорт"
private fun String.isBarbershop() = this.toLowerCase(Locale.getDefault()) == "парикмахерская"
