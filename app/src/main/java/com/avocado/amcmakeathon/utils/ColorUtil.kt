package com.avocado.amcmakeathon.utils

import com.avocado.amcmakeathon.ui.map.PlaceMarker
import com.qwaey.eventpicker.R

fun getColorRes(placeMarker: PlaceMarker): Int {
    return getColorRes(100 * placeMarker.currentFullness / placeMarker.maxFullness)
}

fun getColorRes(value: Int): Int {
    return when (value) {
        in 0..30 -> R.color.color_green
        in 31..60 -> R.color.color_yellow
        in 61..90 -> R.color.color_orange
        else -> R.color.color_red
    }
}