package com.avocado.amcmakeathon.utils

object KeyStore {

    const val SHARED_PREFERENCES_KEY = "SHARED_PREFERENCES_KEY"

    const val FIRST_LAUNCH_KEY = "FIRST_LAUNCH_KEY"
}