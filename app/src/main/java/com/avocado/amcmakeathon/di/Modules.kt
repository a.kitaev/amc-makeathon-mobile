package com.avocado.amcmakeathon.di

import com.avocado.amcmakeathon.ProjectApplication
import com.avocado.amcmakeathon.api.provideOkHttpClient
import com.avocado.amcmakeathon.api.providePlacesCall
import com.avocado.amcmakeathon.api.provideRetrofit
import com.avocado.amcmakeathon.model.interactor.PlaceInteractor
import com.avocado.amcmakeathon.model.providers.ImageProvider
import com.avocado.amcmakeathon.model.providers.ResourceProvider
import com.avocado.amcmakeathon.model.repository.PlaceRepository
import com.avocado.amcmakeathon.model.repository.PreferencesRepository
import com.avocado.amcmakeathon.ui.holdStart.HoldStartViewModel
import com.avocado.amcmakeathon.ui.list.ListViewModel
import com.avocado.amcmakeathon.ui.main.MainViewModel
import com.avocado.amcmakeathon.ui.map.MapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val repositoryModule = module(override = true) {
    single { PreferencesRepository() }
    single { PlaceRepository(get()) }
    factory { provideOkHttpClient() }
    single { provideRetrofit(get()) }
    single { providePlacesCall(get()) }
}

val interactorModule = module(override = true) {
    single { PlaceInteractor(get()) }
}

val providerModule = module(override = true) {
    single { ResourceProvider(ProjectApplication.appInstance.applicationContext) }
    single { ImageProvider() }
}

val viewModelModule = module(override = true) {
    viewModel { HoldStartViewModel(get()) }
    viewModel { MapViewModel() }
    viewModel { ListViewModel() }
    viewModel { MainViewModel(get(), get()) }
}