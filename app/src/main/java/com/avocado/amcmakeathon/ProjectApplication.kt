package com.avocado.amcmakeathon

import android.app.Application
import com.avocado.amcmakeathon.di.interactorModule
import com.avocado.amcmakeathon.di.providerModule
import com.avocado.amcmakeathon.di.repositoryModule
import com.avocado.amcmakeathon.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ProjectApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        appInstance = this
        startKoin {
            androidContext(this@ProjectApplication)
            modules(
                listOf(
                    repositoryModule,
                    interactorModule,
                    providerModule,
                    viewModelModule
                )
            )
        }
    }

    companion object {

        lateinit var appInstance: ProjectApplication
            private set
    }
}