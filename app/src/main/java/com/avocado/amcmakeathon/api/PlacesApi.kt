package com.avocado.amcmakeathon.api

import retrofit2.http.GET

interface PlacesApi {

    @GET("place/getAll")
    suspend fun getPlaces(): Response
}