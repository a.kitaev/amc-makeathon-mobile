package com.avocado.amcmakeathon.api

import com.avocado.amcmakeathon.model.data.Place

data class Response(
    val response: List<Place>
)